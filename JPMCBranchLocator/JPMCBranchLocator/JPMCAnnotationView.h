//
//  JPMCAnnotationView.h
//  JPMCBranchLocator
//
//  Created by Waqas Qureshi on 19/11/2016.
//  Copyright © 2016 Virtusa Polaris. All rights reserved.
//

#import <MapKit/MapKit.h>
#import "JPMCPointAnnotation.h"
@interface JPMCAnnotationView : MKAnnotationView

@end
