//
//  JPMCMapVC.m
//  JPMCBranchLocator
//
//  Created by Waqas Qureshi on 19/11/2016.
//  Copyright © 2016 Virtusa Polaris. All rights reserved.
//

#import "JPMCMapVC.h"

@interface JPMCMapVC () 
{
    CLLocationManager *locationMgr;
    
}



-(void)initializeLocationManager;
@end

@implementation JPMCMapVC
@synthesize list,annotations,_selectedLocation;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.annotations = [NSMutableArray array];
    [self initializeLocationManager];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)initializeLocationManager
{
    locationMgr = [[CLLocationManager alloc] init];
    [locationMgr requestWhenInUseAuthorization];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - Create & Populate Annotations
- (void)fetchATMList:(NSString *)lat longitude:(NSString *)lng
{
    [[JPMCATMLocator sharedInstance] branchesAtLocation:lat longitude:lng withCallback:^(NSMutableArray *atmList, NSError *error) {
        if([atmList count]>0){
            [self.list removeAllObjects];
            self.list = [NSMutableArray arrayWithArray:atmList];
            [self addATMPoints];
        }
        NSLog(@"%@",atmList);
    }];
}

- (void)addATMPoints
{
    for (JPMCLocation *location in self.list) {
        JPMCPointAnnotation *_pAnnotation = [[JPMCPointAnnotation alloc] initWithJPMCLocation:location];
        [self.annotations addObject:_pAnnotation];
    }
    [self.atmMarkerView addAnnotations:self.annotations];
}

#pragma mark - MKMapKit Delegates
- (nullable MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation
{
    if ([annotation isKindOfClass:[MKUserLocation class]]) return nil;
    
    static NSString *annotationViewIdentifier = @"atmViewIdentifier";
    JPMCAnnotationView *annotationView = (JPMCAnnotationView *)[self.atmMarkerView dequeueReusableAnnotationViewWithIdentifier:annotationViewIdentifier];
    
    if(annotationView)
        annotationView.annotation = annotation;
    else
        annotationView = [[JPMCAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:annotationViewIdentifier];
    return annotationView;
}

@end
