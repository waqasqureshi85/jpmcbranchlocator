//
//  ViewController.m
//  JPMCBranchLocator
//
//  Created by Waqas Qureshi on 19/11/2016.
//  Copyright © 2016 Virtusa Polaris. All rights reserved.
//

#import "JPMCLocatorVC.h"

@interface JPMCLocatorVC ()

@end

@implementation JPMCLocatorVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - MKMapKit Delegates
- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(nonnull MKUserLocation *)userLocation
{
    CLLocationCoordinate2D currentLocation = userLocation.location.coordinate;
    mapView.centerCoordinate = currentLocation;
    [mapView setRegion:MKCoordinateRegionMakeWithDistance(currentLocation, 4000, 4000) animated:YES];
    NSString *lat = [[NSString alloc] initWithFormat:@"%g", currentLocation.latitude];
    NSString *lng = [[NSString alloc] initWithFormat:@"%g", currentLocation.longitude];
    NSLog(@"new location %@ , %@",lat,lng);
    [self fetchATMList:lat longitude:lng];
}

- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control
{
    JPMCAnnotationView *annotationView = (JPMCAnnotationView *)view;
    JPMCPointAnnotation *annotation = (JPMCPointAnnotation *)annotationView.annotation;
    //NSLog(@"address %@,%@,%@",annotation._location._address,annotation._location._city,annotation._location._state);
    self._selectedLocation = annotation._location;
    [self performSegueWithIdentifier:@"JPMCLocationDetailSegue" sender:annotation._location];
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([segue.identifier isEqualToString:@"JPMCLocationDetailSegue"]) {
        JPMCLocationDetailController *detailController =  (JPMCLocationDetailController *)[segue destinationViewController];
        JPMCLocation *location = (JPMCLocation *)sender;
        detailController.location = location;
//        NSLog(@"%@",location._locType);
    }

}
@end
