//
//  JPMCLocationDetailController.h
//  JPMCBranchLocator
//
//  Created by Waqas Qureshi on 19/11/2016.
//  Copyright © 2016 Virtusa Polaris. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <JPMCServices/JPMCServices.h>
@interface JPMCLocationDetailController : UIViewController

@property (strong, nonatomic) JPMCLocation      *location;
- (void)loadContent;
@end
