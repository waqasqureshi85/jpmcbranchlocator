//
//  JPMCPointAnnotation.m
//  JPMCBranchLocator
//
//  Created by Waqas Qureshi on 19/11/2016.
//  Copyright © 2016 Virtusa Polaris. All rights reserved.
//

#import "JPMCPointAnnotation.h"

@implementation JPMCPointAnnotation

@synthesize _location;
-(instancetype)initWithJPMCLocation:(JPMCLocation *)loc
{
    self = [super init];
    self._location = loc;
    CLLocationCoordinate2D locCoordinate = CLLocationCoordinate2DMake([_location._lat doubleValue],[_location._lng doubleValue]);
    self.coordinate = locCoordinate;
    self.title = _location._locType;
    self.subtitle = [NSString stringWithFormat:@"%@,%@",_location._address,_location._city];
    return self;
}
@end
