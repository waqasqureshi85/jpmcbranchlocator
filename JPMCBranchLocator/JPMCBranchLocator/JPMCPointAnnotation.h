//
//  JPMCPointAnnotation.h
//  JPMCBranchLocator
//
//  Created by Waqas Qureshi on 19/11/2016.
//  Copyright © 2016 Virtusa Polaris. All rights reserved.
//

#import <MapKit/MapKit.h>
#import <JPMCServices/JPMCServices.h>
@interface JPMCPointAnnotation : MKPointAnnotation
@property (strong, nonatomic) JPMCLocation    *_location;
-(instancetype)initWithJPMCLocation:(JPMCLocation *)loc;
@end
