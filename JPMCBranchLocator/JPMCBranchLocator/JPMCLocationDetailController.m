//
//  JPMCLocationDetailController.m
//  JPMCBranchLocator
//
//  Created by Waqas Qureshi on 19/11/2016.
//  Copyright © 2016 Virtusa Polaris. All rights reserved.
//

#import "JPMCLocationDetailController.h"

@interface JPMCLocationDetailController ()

{
    NSArray *weekDays;
}
@property (weak, nonatomic) IBOutlet UILabel *_bankName;
@property (weak, nonatomic) IBOutlet UILabel *_addressLbl;
@property (weak, nonatomic) IBOutlet UILabel *_atmCountLbl;
@property (nonatomic, strong) IBOutletCollection(UILabel) NSArray *_lobbyHrs;
@property (nonatomic, strong) IBOutletCollection(UILabel) NSArray *_driveUpHrs;

- (void)displayWeeklySchedule:(NSArray *)schedule isLobbyHours:(BOOL)isLobby;
@end

@implementation JPMCLocationDetailController
@synthesize location;
-(void)awakeFromNib
{
    [super awakeFromNib];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    weekDays = [[NSArray alloc] initWithObjects:@"Sunday",@"Monday",@"Tuesday",@"Wednesday",@"Thursday",@"Friday",@"Saturday", nil];
    [self loadContent];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)loadContent
{
    self._bankName.text = location._bank;
    self._addressLbl.text = [NSString stringWithFormat:@"%@,%@,%@ %@",location._address,location._city,location._state,location._zip];
    self._atmCountLbl.text = [NSString stringWithFormat:@"%d",location._totalAtms];
    if([location._lobbyHrs count]>0)
        [self displayWeeklySchedule:location._lobbyHrs isLobbyHours:YES];
    
    if([location._driveUpHrs count]>0)
        [self displayWeeklySchedule:location._driveUpHrs isLobbyHours:NO];
}
- (void)displayWeeklySchedule:(NSArray *)schedule isLobbyHours:(BOOL)isLobby
{
    for (int i=0; i<[schedule count]; i++) {
        NSString *day = [weekDays objectAtIndex:i];
        NSString *timings = [schedule objectAtIndex:i];
        UILabel *label = nil;
        if (isLobby) {
            label = (UILabel *)[self._lobbyHrs objectAtIndex:i];
        }else
            label = (UILabel *)[self._driveUpHrs objectAtIndex:i];
        label.text = [NSString stringWithFormat:@"%@  %@",day, [timings length]>0?timings:@"Closed"];
    }
}
@end
