//
//  JPMCAnnotationView.m
//  JPMCBranchLocator
//
//  Created by Waqas Qureshi on 19/11/2016.
//  Copyright © 2016 Virtusa Polaris. All rights reserved.
//

#import "JPMCAnnotationView.h"

@implementation JPMCAnnotationView

- (instancetype)initWithAnnotation:(id<MKAnnotation>)annotation reuseIdentifier:(nullable NSString *)reuseIdentifier
{
    // The re-use identifier is always nil because these custom pins may be visually different from one another
    self = [super initWithAnnotation:annotation
                     reuseIdentifier:reuseIdentifier];
    
    JPMCPointAnnotation *_pAnnotation = (JPMCPointAnnotation *)annotation;
    // Callout settings - if you want a callout bubble
    self.canShowCallout = YES;
    self.image = [UIImage imageNamed:@"myPinImage"];
    UIButton *detailDisclosure = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
    self.rightCalloutAccessoryView = detailDisclosure;
    UIImageView *iconView;
    if([_pAnnotation._location._locType isEqualToString:@"branch"])
        iconView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bank_icon"]];
    else
        iconView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"atm_icon"]];
    self.leftCalloutAccessoryView = iconView;
    return self;
}

@end
