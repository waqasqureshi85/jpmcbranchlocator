//
//  JPMCMapVC.h
//  JPMCBranchLocator
//
//  Created by Waqas Qureshi on 19/11/2016.
//  Copyright © 2016 Virtusa Polaris. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <JPMCServices/JPMCServices.h>
#import "JPMCAnnotationView.h"
#import "JPMCPointAnnotation.h"
@interface JPMCMapVC : UIViewController <MKMapViewDelegate>

@property (weak, nonatomic) IBOutlet MKMapView *atmMarkerView;
@property (strong, nonatomic) NSMutableArray    *list;
@property (strong, nonatomic) NSMutableArray    *annotations;
@property (strong, nonatomic) JPMCLocation      *_selectedLocation;

- (void)fetchATMList:(NSString *)lat longitude:(NSString *)lng;
- (void)addATMPoints;
@end
