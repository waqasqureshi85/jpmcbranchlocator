//
//  JPMCLocation.m
//  JPMCServices
//
//  Created by Waqas Qureshi on 19/11/2016.
//  Copyright © 2016 Virtusa Polaris. All rights reserved.
//

#import "JPMCLocation.h"
#import "NSDictionary+Extra.h"
@implementation JPMCLocation

@synthesize _state,_locType,_label,_address,_city,_zip,_name,_lat,_lng,
_bank,_type,_lobbyHrs,_driveUpHrs,_totalAtms,_phone,_distance;

- (instancetype)initWithAttributes:(NSDictionary *)data
{
    if (data == nil) return nil;
    if ((self = [super init])) {
        self._state = [data stringForKey:@"state"];
        self._locType = [data stringForKey:@"locType"];
        self._label = [data stringForKey:@"label"];
        self._address = [data stringForKey:@"address"];
        self._city = [data stringForKey:@"city"];
        self._zip = [data stringForKey:@"zip"];
        self._name = [data stringForKey:@"name"];
        self._lat = [data stringForKey:@"lat"];
        self._lng = [data stringForKey:@"lng"];
        self._bank = [data stringForKey:@"bank"];
        self._type = [data stringForKey:@"type"];
        self._lobbyHrs = [NSMutableArray array];
        self._driveUpHrs = [NSMutableArray array];
        self._lobbyHrs = [data objectForKey:@"lobbyHrs"];
        self._driveUpHrs = [data objectForKey:@"driveUpHrs"];
        self._totalAtms = [[data stringForKey:@"atms"] intValue];
        self._phone = [[data stringForKey:@"phone"] intValue];
        self._distance = [data stringForKey:@"distance"];
    }
    return self;
}
@end
