//
//  JPMCATMLocator.m
//  JPMCServices
//
//  Created by Waqas Qureshi on 18/11/2016.
//  Copyright © 2016 Virtusa Polaris. All rights reserved.
//

#import "JPMCATMLocator.h"
#import "JPMCServicesAPI.h"
#import "JPMCLocation.h"
@implementation JPMCATMLocator


+ (JPMCATMLocator *)sharedInstance
{
    static JPMCATMLocator *_sharedInstance = nil;
    static dispatch_once_t oncePredicate;
    dispatch_once(&oncePredicate, ^{
        _sharedInstance = [[self alloc] init];
    });
    
    return _sharedInstance;
}
#pragma mark- ############-- Branches Locator Service --############
// This delegate assign this task to Main API Manager.
-(void)branchesAtLocation:(NSString*)lat longitude:(NSString*)lng withCallback:(void(^)(NSMutableArray *atmList, NSError* error))callback
{
    [[JPMCServicesAPI sharedInstance] callGETAPI:[NSString stringWithFormat:@"lat=%@&lng=%@",lat,lng] method:@"location/list.action" withCallback:^(NSError *error, NSDictionary *response) {
        if (response) {
            NSArray *locations = [response objectForKey:@"locations"];
            NSMutableArray *jpmcLocations = [NSMutableArray array];
            for (NSDictionary *location in locations) {
                JPMCLocation *_jpmcLoc = [[JPMCLocation alloc] initWithAttributes:location];
                [jpmcLocations addObject:_jpmcLoc];
            }
            callback(jpmcLocations,error);
        }else{
            callback(nil,nil);
        }
        
    }];
}
@end
