//
//  JPMCServices.h
//  JPMCServices
//
//  Created by Waqas Qureshi on 18/11/2016.
//  Copyright © 2016 Virtusa Polaris. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for JPMCServices.
FOUNDATION_EXPORT double JPMCServicesVersionNumber;

//! Project version string for JPMCServices.
FOUNDATION_EXPORT const unsigned char JPMCServicesVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <JPMCServices/PublicHeader.h>

//ATM Locator Manager
#import <JPMCServices/JPMCATMLocator.h>
#import <JPMCServices/JPMCLocation.h>
