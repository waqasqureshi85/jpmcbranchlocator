//
//  JPMCLocation.h
//  JPMCServices
//
//  Created by Waqas Qureshi on 19/11/2016.
//  Copyright © 2016 Virtusa Polaris. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JPMCLocation : NSObject

@property (strong, nonatomic) NSString      *_state;
@property (strong, nonatomic) NSString      *_locType;
@property (strong, nonatomic) NSString      *_label;
@property (strong, nonatomic) NSString      *_address;
@property (strong, nonatomic) NSString      *_city;
@property (strong, nonatomic) NSString      *_zip;
@property (strong, nonatomic) NSString      *_name;
@property (strong, nonatomic) NSString      *_lat;
@property (strong, nonatomic) NSString      *_lng;
@property (strong, nonatomic) NSString      *_bank;
@property (strong, nonatomic) NSString      *_type;
@property (strong, nonatomic) NSMutableArray*   _lobbyHrs;
@property (strong, nonatomic) NSMutableArray*   _driveUpHrs;
@property (readwrite, nonatomic) int        _totalAtms;
@property (readwrite, nonatomic) int        _phone;
@property (strong, nonatomic) NSString      *_distance;

- (instancetype)initWithAttributes:(NSDictionary *)data;
@end
