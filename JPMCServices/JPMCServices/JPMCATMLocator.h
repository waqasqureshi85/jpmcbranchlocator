//
//  JPMCATMLocator.h
//  JPMCServices
//
//  Created by Waqas Qureshi on 18/11/2016.
//  Copyright © 2016 Virtusa Polaris. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JPMCATMLocator : NSObject
+ (JPMCATMLocator *)sharedInstance;

-(void)branchesAtLocation:(NSString*)lat longitude:(NSString*)lng withCallback:(void(^)(NSMutableArray *atmList, NSError* error))callback;

                                                                                
@end
