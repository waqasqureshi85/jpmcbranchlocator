//
//  JPMCServicesAPI.h
//  JPMCServices
//
//  Created by Waqas Qureshi on 18/11/2016.
//  Copyright © 2016 Virtusa Polaris. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFNetworking/AFNetworking.h"
@interface JPMCServicesAPI : NSObject

+ (JPMCServicesAPI *)sharedInstance;

// GET API with Query String
-(void)callGETAPI:(NSString*)params method:(NSString*)method withCallback:(void (^)(NSError *error, NSDictionary *response))callback;
// create POST,PUT & DELETE API methods...
@end
