//
//  JPMCServicesAPI.m
//  JPMCServices
//
//  Created by Waqas Qureshi on 18/11/2016.
//  Copyright © 2016 Virtusa Polaris. All rights reserved.
//

#import "JPMCServicesAPI.h"
#import "JPMCHeader.h"
@interface JPMCServicesAPI ()
{
    
}
// send GET request with Query String...
-(void)callGETReqWithQS:(NSString*)params method:(NSString*)method
           withCallback:(void (^)(NSError *error, NSDictionary *response))callback;
@end

@implementation JPMCServicesAPI


+ (JPMCServicesAPI *)sharedInstance
{
    static JPMCServicesAPI *_sharedInstance = nil;
    static dispatch_once_t oncePredicate;
    dispatch_once(&oncePredicate, ^{
        _sharedInstance = [[self alloc] init];
    });
    
    return _sharedInstance;
}
-(void)callGETReqWithQS:(NSString*)params method:(NSString*)method
           withCallback:(void (^)(NSError *error, NSDictionary *response))callback
{
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [manager GET:[NSString stringWithFormat:@"%@%@?%@",kBaseUrl,method,params] parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSDictionary *atmList = (NSDictionary *)responseObject;
        callback(nil,atmList);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        callback(error,nil);
    }];
}

-(void)callGETAPI:(NSString*)params method:(NSString*)method
     withCallback:(void (^)(NSError *error, NSDictionary *response))callback
{
    [self callGETReqWithQS:params method:method withCallback:^(NSError *error, NSDictionary *response) {
        callback(error, response);
    }];
}
@end
